from xmlrpc.server import SimpleXMLRPCServer
import pypandoc

def file_upload(filedata,pc,namafile):
    nama = "uploadan_pc_"+ pc +"_"+ namafile +".docx"
    npdf = "print_pc_"+ pc +"_"+ namafile +".pdf"
    with open(nama,'wb') as handle:
        data1=filedata.data
        handle.write(data1)
    output = pypandoc.convert_file(nama, 'pdf', outputfile=npdf)
    assert output == ""
    return True

server = SimpleXMLRPCServer(("127.0.0.1",9999))
print ("Listening on port 9999")
server.register_function(file_upload,"file_upload")
server.serve_forever()
