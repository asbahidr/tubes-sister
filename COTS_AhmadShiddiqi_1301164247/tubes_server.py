from xmlrpc.server import SimpleXMLRPCServer
import pypandoc
import os

access_rights = 0o755
default_path = "/home/fahmifauzan/Documents/ahmad/"

def file_upload(filedata,namafolder):
    with open("uploadan_pc1.docx",'wb') as handle:
        data1=filedata.data
        handle.write(data1)
    path = "/home/fahmifauzan/Documents/ahmad/"+namafolder+"/"
    try:
        os.mkdir(path, access_rights)
    except OSError:
        print("Pembuatan folder %s gagal" %path)
    else:
        print("Folder berhasil dibuat %s " %path)
    output = pypandoc.convert_file('uploadan_pc1.docx', 'pdf', outputfile="uploadan_pc1_print.pdf")
    os.rename(default_path+"uploadan_pc1_print.pdf", default_path+namafolder+"/uploadan_pc1_print.pdf")
    assert output == ""
    return True

server = SimpleXMLRPCServer(("127.0.0.1",9999))
print ("Listening on port 9999")
server.register_function(file_upload,"file_upload")
server.serve_forever()
