from xmlrpc.server import SimpleXMLRPCServer

import pypandoc

def file_upload(filedata,namafile):
    nf = 'uploadan_pc1_'+ namafile +'.docx'
    nfx = 'uploadan_pc1_'+ namafile +'.pdf'
    with open(nf,'wb') as handle:
        data1=filedata.data
        handle.write(data1)
    output = pypandoc.convert_file(nf, 'pdf', outputfile=nfx)
    assert output == ""
    return True

server = SimpleXMLRPCServer(("127.0.0.1",9999))
print ("Listening on port 9999")
server.register_function(file_upload,"file_upload")
server.serve_forever()
