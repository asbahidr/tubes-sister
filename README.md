# Kelompok-4 Tubes Sister Printing Online

#### Nama Anggota:
1. Ahmad Shiddiqi 1301164247
2. Asbahid Rifqi Bilgiflah 1301164090
3. Irfandio Daffa Agustantio 1301160175
4. M Fahmi Fauzan 1301164078
5. Salma Fauzia Sausan 1301164442

#### Requirement

*  Pyhton 3+
*  Pypandoc
*  XMLRPC

#### Printing Online
Printing online merupakan aplikasi printingyang ditujukan untuk membantu masalah mahasiswa untuk melakukan printing. Solusi yang diberikan adalah dengan membuat sebuah aplikasi yang dapat mengirimkan file dari beberapa user kepada server utama dan saat file utama diterima di server maka akan dapat melakukan proses printing. Dan hasil printing dapat langsung diantarkan pada customer

#### FTP
merupakan sebuah protokol yang dapat bertukar file dalam suatu jaringan yagn menggunakan koneksi TCP. Pada FTP terdapat FTP server yang berguna untuk menerima paket dan FTP client yang mengirimkan paket.

#### RPC(Remote Procedure Call)
merupakan metode yang dapat memungkinkan untuk mengakses prosedur yang ada pada perangkat lain. Metode ini bekerja dengan cara server membuka socket dan menunggu client meminta prosedur yang ada pada server. Client dapat melakukan request jika client tidak dapat mengetahui pot mana yang harus dihubungi. Penggunaan metode RPC pada kasus yang kami pakai adalah saat cutomer mengirimkan file yang ingin diprint.

#### Server dan Client
![](./images/client_and_server.png)

#### How to Run
1. Client memasukkan nama file yang akan di print
2. Server akan memproses file menjadi pdf dan menyimpannya
3. Client dapat melakukan print berulang kali hingga memilih no

#### Penambahan Feature:
1. Ahmad Shiddiqi 1301164247
* Memasukan hasil upload client ke dalam folder baru.
2. Asbahid Rifqi Bilgiflah 1301164090
* Menambahkan feature untuk membedakan pc mana yang mengupload (setiap pc bisa diberi nama sendiri) dan juga membuat upload memunculkan nama file, sebelum "uploadan_pc1.docx", sesudah ditambahkan fitur "uploadan_pc_bahid_namafile.docx".
3. Irfandio Daffa Agustantio 1301160175
* Mencatat history transaksi yang telah dilakukan client ke dalam file csv
4. M Fahmi Fauzan 1301164078
* Menambahkan feature untuk dapat mengupload 2 file dalam fomrmat .docx sekaligus sebelum di convert menjadi file .pdf lalu di print 
5. Salma Fauzia Sausan 1301164442
* Menambahkan feature untuk mengecek file sudah pernah diprint atau belum, dengan cara mengecek file yang diinput dengan file yang ada di server apakah nama file tersebut sama atau tidak. Jika sama akan keluar print yang sudah diprint sebelumnya.