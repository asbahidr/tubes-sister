import xmlrpc.client
import pandas as pd
import numpy as np

proxy = xmlrpc.client.ServerProxy("http://127.0.0.1:9999/")     #IP yang digunakan
j = 'yes'
history = []     #untuk menampung transaksi
i = 1

"""Untuk mengisi data nama user
def data_nama_user():
    print("Masukan nama: ")
    nama = input()"""

"""Untuk mengisi data alamat customer"""
def data_alamat_customer():
    print('Masukan alamat: ')
    alamat = input()

"""perulangan untuk melakukan proses upload file dan mendata transaksi"""
while (j == 'yes'):
    print('Masukan nama: ')
    a = input()
    y = data_alamat_customer()
    print ("Masukkan nama file: ")
    x = input() + '.docx'

    """Digunakan untuk menampung file yang diupload"""
    with open(x,'rb') as handle:
        data = xmlrpc.client.Binary(handle.read())

    proxy.file_upload(a, data, x)   #file yang diupload akan diterima dengan format nama sesuai pada fungsi file.upload
    print('Success!')

    print ("Upload lagi? yes/no")
    j = input()

    history.append([a,y]) #menyimpan data user ke dalam variable history

"""Fungsi yang digunakan untuk menghasilkan file csv yang berisi transaksi yang telah dilakukan"""
def cetak_history(x):
    d = np.asarray(x)
    d.tofile('history-transaksi.csv', sep = '\n', format = '%s')
    
# note: pada hasil file csv masih terdapat bug dimana data yang berhasil tersimpan hanya nama dari customer saja.

cetak_history(history)
    
    
