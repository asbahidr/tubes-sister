from xmlrpc.server import SimpleXMLRPCServer
import pypandoc

"""fungsi untuk memberikan informasi (nama customer, nama file) saat diupload"""
def file_upload(namauser, filedata,namafile): 
    nfile = namauser+'uploadan_pc1_'+ namafile +'.docx' #menghasilkan file dengan format docx
    nfx = namauser+'uploadan_pc1_'+ namafile +'.pdf'    #menghasilkan file dengan format yang telah diubah ke bentuk pdf
    with open(nfile,'wb') as handle:
        data1=filedata.data
        handle.write(data1)
    output = pypandoc.convert_file(nfile, 'pdf', outputfile=nfx) 
    assert output == ""
    return True

server = SimpleXMLRPCServer(("127.0.0.1",9999))
print ("Listening on port 9999")
server.register_function(file_upload,"file_upload")
server.serve_forever()
