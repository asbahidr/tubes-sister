from xmlrpc.server import SimpleXMLRPCServer
import pypandoc

def file_upload(filedata):
    with open("uploadan_pc1.docx",'wb') as handle:
        data1=filedata.data
        handle.write(data1)
    output = pypandoc.convert_file('uploadan_pc1.docx', 'pdf', outputfile="uploadan_pc1_print.pdf")
    assert output == ""
    return True

server = SimpleXMLRPCServer(("localhost",9999))
print ("Listening on port 9999")
server.register_function(file_upload,"file_upload")
server.serve_forever()
